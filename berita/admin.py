from django.contrib import admin
from .models import Kategori, Berita


class BeritaAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'kategori', 'title', 'content', 'created_at')


admin.site.register(Kategori)
admin.site.register(Berita, BeritaAdmin)