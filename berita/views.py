from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from berita.models import Berita



class Landing(View):

    def get(self, request):
        template_name = "berita/home.html"
        data = {
            "data": Berita.objects.all(),
            "label": "List Berita Mandalika Portal"
        }
        return render(request, template_name, data)


class DetailViewBerita(View):
    def get(self, request, pk):
        template_name = "berita/detail.html"
        data = {
            "berita": get_object_or_404(Berita, pk=pk),
            "id": pk
        }

        return render(request, template_name, data)