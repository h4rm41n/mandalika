from django.contrib import admin
from django.urls import path
from berita.views import Landing, DetailViewBerita


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Landing.as_view()),
    path('detail/<int:pk>', DetailViewBerita.as_view()),
]
